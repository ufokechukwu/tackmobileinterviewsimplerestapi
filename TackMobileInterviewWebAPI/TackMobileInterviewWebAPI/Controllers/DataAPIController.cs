﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TackMobileInterviewWebAPI.Models;

namespace TackMobileInterviewWebAPI.Controllers
{
    [RoutePrefix("api/v1/data")]
    public class DataAPIController : ApiController
    {
        [HttpGet]
        [Route("images")]
        public IHttpActionResult HTTPGetAllCourses()
        {
            var _data = ImageDataDTO.getDefaultValues();
            
            return Ok(_data);
        }
    }
}
