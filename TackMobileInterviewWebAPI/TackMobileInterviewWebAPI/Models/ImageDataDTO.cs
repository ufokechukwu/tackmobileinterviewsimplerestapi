﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TackMobileInterviewWebAPI.Models
{
    public class ImageDataDTO
    {
        public string image_name { get; set; }
        public string image_url { get; set; }
        public string image_description { get; set; }



        public static List<ImageDataDTO> getDefaultValues()
        {
            var _defaults_data = new List<ImageDataDTO>();

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Outer Space",
                image_description = "Cool out of space experience",
                image_url = "https://i.ytimg.com/vi/lt0WQ8JzLz4/maxresdefault.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Space Craft",
                image_description = "The US. Navy Space Craft",
                image_url = "https://s-media-cache-ak0.pinimg.com/originals/16/ca/b8/16cab816a273ee668f7154c6598ba7c7.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Vulcan D'kyr",
                image_description = "Vulcan Science Vessel",
                image_url = "https://s-media-cache-ak0.pinimg.com/736x/dc/b7/5f/dcb75f0cb18ea305a4a89a7584446b89.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Space Wallpaper",
                image_description = "Welcome to Outter Space",
                image_url = "https://newevolutiondesigns.com/images/freebies/space-wallpaper-29.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Study Genius",
                image_description = "Life Beyond Earth",
                image_url = "https://s-media-cache-ak0.pinimg.com/originals/fc/e3/89/fce389769b356278f62b40e9184da446.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Space Man",
                image_description = "Astronaut Space Crawling",
                image_url = "https://s-media-cache-ak0.pinimg.com/736x/2c/e0/5f/2ce05fa3c0a2531fb2c72df32c63eba5.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Golden Globe",
                image_description = "The Earth and its beauty...",
                image_url = "https://s-media-cache-ak0.pinimg.com/736x/db/b2/85/dbb285915a1450e71a3f63e12f7c15af.jpg"
            });

            _defaults_data.Add(new ImageDataDTO
            {
                image_name = "Cosmos",
                image_description = "the Galaxy and its beauty.",
                image_url = "https://s-media-cache-ak0.pinimg.com/736x/bc/61/99/bc6199ca8f5c94c3a8409f4367b1bced.jpg"
            });

            return _defaults_data;
        }
    }

    
}